//this is my include file. It has my function prototypes and my struct node set up
//it also defines MYNULL to be 0
//I called it gcode.h so that way I could include a little G code into each of my files :-)

#define MYNULL 0

struct node{
   int data;
   int next;
   int valid;
};

void init(struct node* LL);

int add(struct node* LL, int number);

void print(struct node* LL);

void delete(struct node* LL, int number);

int search(struct node* LL, int number);

int get_node(struct node* LL);

void release_node(struct node* LL, int indexnumber);
