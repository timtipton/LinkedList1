//this is my init function. It will initialize LL to be an emply linked list.
//this function takesa one argument and has no return value.
//side effects include setting all the valid flags for every node besides the sentinel node to 0.

#include "gcode.h"

void init(struct node* LL){
   LL[0].next=MYNULL;//creates the sentinel node and sets its valid flag to 1
   LL[0].valid=1;
   for(int i=1; i<=99; i++){//loop to set all the rest of the valid flags to 0.
      LL[i].valid=0;
   }
}
