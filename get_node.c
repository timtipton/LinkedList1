//this is the get_node function.
//this function iterates through the valid field of the linked list and finds where there is 
//a valid field set to 0. It changes that valid field to 1 and returns the index of 
//that field. If there are no emply nodes than the function returns MYNULL.

#include "gcode.h"

int get_node(struct node* LL){
   for(int i=1; i<=99; i++){
      if(LL[i].valid == 0){
         LL[i].valid=1;
         return i;
      }
    } 
    if(LL[99].valid==1) 
         return MYNULL;
}
