//this is my add function
//this adds a number to the data field of the linked list
//the number will be added in order from smallest to biggest so
//that my linked list will be sorted.
//the function returns 1 if successful and 0 if not and I guess you could call
//having a new node in the linked list a side effect if you want.
//#include<stdlib.h>
#include<stdio.h>
#include "gcode.h"

int add(struct node* LL, int number){

   int new=get_node(LL);//sets a variable to keep track of the index of the new node.
   LL[new].data=number;
   int previous_node=0;//variable to keep track of the previous node. Needed to insert number before the current node.
   int current_node=LL[0].next;//keeps track of the current node as the list is being traversed

   while(current_node!=MYNULL){
      if(LL[current_node].data>number){//looking for the first number bigger than "number"
         LL[previous_node].next=new;//points the previous node to the new node
         LL[new].next=current_node;//points the new node to the current node (basically sliding it into the middle like when your dog wants to cuddle with you and your significant other)
         return 1;
      }
      else{
         current_node=LL[current_node].next;//these two lines travers the list if the current node isn't mynull
         previous_node=LL[previous_node].next;
      }  
   }
   LL[previous_node].next=new;//these next three lines add a number to the end of the list.   
   LL[new].next=current_node;
   LL[new].data=number;
   return 1;
 
  if(LL[99].next==MYNULL)//returns 0 if the list is full.
     return 0;    
}

