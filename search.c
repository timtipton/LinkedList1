//``this is my search function.
//it searches my linked list for a node containing a certain number.
//it returns 1 if the node is found and 0 if not.
//this will be useful for things like the add function.
//no side effects.
#include<stdio.h>
#include "gcode.h"

int search(struct node* LL, int number){
   int current=LL[0].next;
   while(current!=MYNULL){
      if(LL[current].data==number){ 
        return 1;
      }
      else{
       current=LL[current].next;
      }
   }
   return 0;
} 

