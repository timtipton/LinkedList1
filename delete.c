//this is my delete function. This function deletes a number from the list.
// there is no return value and no side effects.

#include "gcode.h"

void delete(struct node* LL, int number){
   int current=LL[0].next;//sets the current variable to be the index of the next node in the list
   int previous=0;//sets previous to the index of the sentinel node.

   while(current!=MYNULL){
      if(LL[current].data==number){//if we found the number to delete
         LL[previous].next=LL[current].next;//deletes that node from the list
         release_node(LL, current);//clears the valid flag of the node we just deleted
         return;
      }
      else{
         previous=current;//these two lines travers the list
         current=LL[current].next;
      }
   }
}
