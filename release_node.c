//this is my release_node function. It basically just sets the valid flag of a deleted node to 0.
//the function has no return value and will be used in conjunction with the delete function.
//no side effects, which is the opposite of every medicine you see on tv.

#include "gcode.h"

void release_node(struct node* LL, int indexnumber){
   LL[indexnumber].valid=0;
}

